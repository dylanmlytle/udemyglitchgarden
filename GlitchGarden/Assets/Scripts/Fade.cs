﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

    public float fadeTime;

    private Image fadePanel;
    private Color currentColor = Color.black;

	void Start ()
    {
        fadePanel = GetComponent<Image>();
	}
	
	void Update ()
    {
		if(Time.timeSinceLevelLoad < fadeTime)
        {
            float alphaChange = Time.deltaTime / fadeTime;
            currentColor.a -= alphaChange;
            fadePanel.color = currentColor;
        }
        else
        {
            gameObject.SetActive(false);
        }
	}
}
